import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension

val springBootVersion by extra("2.6.4")

plugins {
	`java-library`
}

buildscript {
	val springBootVersion by extra("2.6.4")
	repositories {
		mavenCentral()
	}
	dependencies {
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
	}
}

subprojects {
	group = "ru.prestu"
	version = "1.0"

	repositories {
		mavenCentral()
	}

	apply(plugin = "java")
	apply(plugin = "org.springframework.boot")
	apply(plugin = "io.spring.dependency-management")

	the<DependencyManagementExtension>().apply {
		imports {
			mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
		}
	}

	val lombokVersion = "1.18.22"
	val mapstructVersion = "1.4.2.Final"
	dependencies {
		// Lombok
		compileOnly("org.projectlombok:lombok:${lombokVersion}")
		annotationProcessor("org.projectlombok:lombok:${lombokVersion}")
		compileOnly("org.mapstruct:mapstruct:${mapstructVersion}")
		annotationProcessor("org.mapstruct:mapstruct-processor:${mapstructVersion}")
		testCompileOnly("org.projectlombok:lombok:${lombokVersion}")
		testAnnotationProcessor("org.projectlombok:lombok:${lombokVersion}")
	}

	tasks {
		withType<Test> {
			useJUnitPlatform()
		}
		withType<JavaCompile> {
			sourceCompatibility = "17"
			targetCompatibility = "17"
		}
	}
}
