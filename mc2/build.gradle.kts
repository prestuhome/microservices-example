import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

description = "MC2 microservice"

dependencies {
	implementation(project(":resource-model"))
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-websocket")
	implementation("org.springframework.kafka:spring-kafka")
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
	imageName = "prestu/" + rootProject.name + '-' + project.name
}
