package ru.prestu.microservices.mc2.service;

import ru.prestu.microservices.model.MessageDto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MessagePublisherService {

	KafkaTemplate<String, MessageDto> kafkaTemplate;
	String topicName;

	public MessagePublisherService(
			final KafkaTemplate<String, MessageDto> kafkaTemplate,
			@Value("${kafka.topicName}") final String topicName
	) {
		this.kafkaTemplate = kafkaTemplate;
		this.topicName = topicName;
	}

	public void publishMessage(final MessageDto message) {
		kafkaTemplate.send(topicName, message);
	}
}
