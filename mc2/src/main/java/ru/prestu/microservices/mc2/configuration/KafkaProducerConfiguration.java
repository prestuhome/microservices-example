package ru.prestu.microservices.mc2.configuration;

import ru.prestu.microservices.model.MessageDto;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.util.HashMap;

@EnableKafka
@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE)
public class KafkaProducerConfiguration {

	@Value(value = "${kafka.url}")
	private String url;

	@Bean
	public ProducerFactory<String, MessageDto> messageProducerFactory() {
		val configProps = new HashMap<String, Object>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, url);
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	@Bean
	public KafkaTemplate<String, MessageDto> messageKafkaTemplate() {
		return new KafkaTemplate<>(messageProducerFactory());
	}
}
