package ru.prestu.microservices.mc2.controller;

import ru.prestu.microservices.mc2.service.MessageModifierService;
import ru.prestu.microservices.mc2.service.MessagePublisherService;
import ru.prestu.microservices.model.MessageDto;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Controller
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class WebSocketController {

	MessageModifierService messageModifierService;
	MessagePublisherService messagePublisherService;

	@MessageMapping("/message")
	public void getMessage(final MessageDto message) {
		messageModifierService.modifyMessage(message);
		messagePublisherService.publishMessage(message);
	}
}
