import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

description = "MC3 microservice"

dependencies {
	val retrofitVersion = "2.9.0"

	implementation(project(":resource-model"))
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.kafka:spring-kafka")
	implementation("com.squareup.retrofit2:retrofit:${retrofitVersion}")
	implementation("com.squareup.retrofit2:converter-jackson:${retrofitVersion}")
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
	imageName = "prestu/" + rootProject.name + '-' + project.name
}
