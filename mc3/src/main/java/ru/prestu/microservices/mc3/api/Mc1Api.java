package ru.prestu.microservices.mc3.api;

import ru.prestu.microservices.model.MessageDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Mc1Api {

	@POST("/message")
	Call<Void> sendMessage(@Body final MessageDto message);
}
