package ru.prestu.microservices.mc3.configuration;

import ru.prestu.microservices.mc3.api.Mc1Api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Mc1ApiConfiguration {

	@Value(value = "${mc1.url}")
	String url;

	@Bean
	public OkHttpClient httpClient() {
		return new OkHttpClient.Builder().build();
	}

	@Bean
	public Mc1Api api() {
		return new Retrofit.Builder()
				.baseUrl(url)
				.addConverterFactory(JacksonConverterFactory.create())
				.client(httpClient())
				.build()
				.create(Mc1Api.class);
	}
}
