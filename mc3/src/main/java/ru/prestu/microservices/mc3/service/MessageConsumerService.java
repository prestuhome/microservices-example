package ru.prestu.microservices.mc3.service;

import ru.prestu.microservices.model.MessageDto;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MessageConsumerService {

	MessageModifierService messageModifierService;
	MessagePublisherService messagePublisherService;

	@KafkaListener(topics = "#{'${kafka.topicName}'}", groupId = "#{'${kafka.groupId}'}")
	public void consumeMessage(MessageDto message) {
		messageModifierService.modifyMessage(message);
		messagePublisherService.publishMessage(message);
	}
}
