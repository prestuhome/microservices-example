package ru.prestu.microservices.mc3.service;

import ru.prestu.microservices.mc3.api.Mc1Api;
import ru.prestu.microservices.model.MessageDto;

import org.springframework.stereotype.Service;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MessagePublisherService {
	
	Mc1Api api;
	
	@SneakyThrows
	public void publishMessage(final MessageDto message) {
		api.sendMessage(message).execute();
	}
}
