package ru.prestu.microservices.mc3.service;

import ru.prestu.microservices.model.MessageDto;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MessageModifierService {

	public void modifyMessage(final MessageDto messageDto) {
		messageDto.setMc3Timestamp(new Date());
	}
}
