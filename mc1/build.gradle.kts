import org.springframework.boot.gradle.tasks.bundling.BootBuildImage

description = "MC1 microservice"

dependencies {
	val mariadbVersion = "2.7.5"
	val flywayVersion = "8.5.4"

	implementation(project(":resource-model"))
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-aop")
	implementation("org.springframework:spring-websocket")
	implementation("org.springframework:spring-messaging")
	runtimeOnly("org.mariadb.jdbc:mariadb-java-client:${mariadbVersion}")
	runtimeOnly("org.flywaydb:flyway-mysql:${flywayVersion}")
	runtimeOnly("org.flywaydb:flyway-core:${flywayVersion}")
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
	imageName = "prestu/" + rootProject.name + '-' + project.name
}
