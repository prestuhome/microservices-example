package ru.prestu.microservices.mc1.repository;

import ru.prestu.microservices.mc1.model.domain.Message;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {

}
