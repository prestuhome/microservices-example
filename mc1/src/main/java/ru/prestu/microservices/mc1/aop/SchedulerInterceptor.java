package ru.prestu.microservices.mc1.aop;

import ru.prestu.microservices.mc1.component.Mc1WebSocketClient;
import ru.prestu.microservices.mc1.model.domain.ExecutionSession;
import ru.prestu.microservices.mc1.repository.ExecutionSessionRepository;
import ru.prestu.microservices.model.MessageDto;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SchedulerInterceptor {

	ExecutionSessionRepository executionSessionRepository;
	Mc1WebSocketClient webSocketClient;
	AtomicInteger sessionIdReference = new AtomicInteger();

	@Pointcut("execution(* ru.prestu.microservices.mc1.component.SchedulerStateChecker.checkSchedulerStateOnStart(..))")
	public void monitorSchedulerStart() {
	}

	@AfterReturning("monitorSchedulerStart()")
	public void onStart() {
		val sessionId = executionSessionRepository.save(initializeExecutionSession()).getId();
		sessionIdReference.set(sessionId);
		log.info("Execution is started: session_id = {}", sessionId);
	}

	@Pointcut("execution(* ru.prestu.microservices.mc1.component.SchedulerStateChecker.checkSchedulerStateOnStop(..))")
	public void monitorSchedulerStop() {
	}

	@AfterReturning("monitorSchedulerStop()")
	public void onStop() {
		executionSessionRepository.findById(sessionIdReference.get()).ifPresent(it -> {
			it.setEndTime(Instant.now().toEpochMilli());
			val session = executionSessionRepository.save(it);
			log.info("Execution is stopped: time = {} ms, message amount = {}", session.getEndTime() - session.getStartTime(), session.getMessageAmount());
		});
	}

	@Pointcut("execution(* ru.prestu.microservices.mc1.service.MessageService.createMessage())")
	public void monitorSchedulerExecution() {
	}

	@SneakyThrows
	@AfterReturning(pointcut = "monitorSchedulerExecution()", returning = "messageDto")
	public void onTaskExecution(final MessageDto messageDto) {
		val sessionId = sessionIdReference.get();
		messageDto.setSessionId(sessionId);
		val stompSession = webSocketClient.connect().get();
		webSocketClient.sendMessage(stompSession, messageDto);
		executionSessionRepository.incrementMessageAmount(sessionId);
	}

	public ExecutionSession initializeExecutionSession() {
		val session = new ExecutionSession();
		session.setStartTime(Instant.now().toEpochMilli());
		session.setMessageAmount(0);
		return session;
	}
}
