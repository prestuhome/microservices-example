package ru.prestu.microservices.mc1.component;

import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStartedException;
import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStoppedException;

import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledFuture;

@Component
public class SchedulerStateChecker {

	public void checkSchedulerStateOnStart(final ScheduledFuture<?> scheduledFuture) throws SchedulerIsAlreadyStartedException {
		if (!executionIsStopped(scheduledFuture)) {
			throw new SchedulerIsAlreadyStartedException();
		}
	}

	public void checkSchedulerStateOnStop(final ScheduledFuture<?> scheduledFuture) throws SchedulerIsAlreadyStoppedException {
		if (executionIsStopped(scheduledFuture)) {
			throw new SchedulerIsAlreadyStoppedException();
		}
	}

	private boolean executionIsStopped(final ScheduledFuture<?> scheduledFuture) {
		return scheduledFuture == null || scheduledFuture.isCancelled();
	}
}
