package ru.prestu.microservices.mc1.model.exception;

public class SchedulerIsAlreadyStoppedException extends Exception {

	public SchedulerIsAlreadyStoppedException() {
		super("Scheduler is already stopped");
	}
}
