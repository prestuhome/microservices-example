package ru.prestu.microservices.mc1.service;

import ru.prestu.microservices.mc1.mapper.MessageMapper;
import ru.prestu.microservices.mc1.repository.MessageRepository;
import ru.prestu.microservices.model.MessageDto;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MessageService {

	MessageRepository messageRepository;
	MessageMapper messageMapper;

	public MessageDto createMessage() {
		val messageDto = new MessageDto();
		messageDto.setMc1Timestamp(new Date());
		return messageDto;
	}

	@Transactional
	public void saveMessage(final MessageDto message) {
		message.setEndTimestamp(new Date());
		Optional.of(message)
				.map(messageMapper::mapToEntity)
				.map(messageRepository::save);
	}
}
