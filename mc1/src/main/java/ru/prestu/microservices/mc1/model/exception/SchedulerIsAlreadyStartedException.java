package ru.prestu.microservices.mc1.model.exception;

public class SchedulerIsAlreadyStartedException extends Exception {

	public SchedulerIsAlreadyStartedException() {
		super("Scheduler is already started");
	}
}
