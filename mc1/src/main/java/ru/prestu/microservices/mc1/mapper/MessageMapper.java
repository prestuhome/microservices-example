package ru.prestu.microservices.mc1.mapper;

import ru.prestu.microservices.mc1.model.domain.Message;
import ru.prestu.microservices.model.MessageDto;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MessageMapper {

	Message mapToEntity(MessageDto messageDto);
}
