package ru.prestu.microservices.mc1.model.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = Message.TABLE_NAME)
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Message {

	public static final String TABLE_NAME = "message";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	Integer id;
	@Column(name = "session_id", nullable = false)
	Integer sessionId;
	@Column(name = "mc1_timestamp", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	Date mc1Timestamp;
	@Column(name = "mc2_timestamp", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	Date mc2Timestamp;
	@Column(name = "mc3_timestamp", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	Date mc3Timestamp;
	@Column(name = "end_timestamp", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	Date endTimestamp;
}
