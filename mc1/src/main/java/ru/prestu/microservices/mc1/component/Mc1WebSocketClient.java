package ru.prestu.microservices.mc1.component;

import ru.prestu.microservices.model.MessageDto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.util.Collections;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Mc1WebSocketClient {

	WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
	ObjectMapper objectMapper;
	String mc2Host;
	Integer mc2Port;

	public Mc1WebSocketClient(
			final @Value("${mc2Host}") String mc2Host,
			final @Value("${mc2Port}") Integer mc2Port,
			final ObjectMapper objectMapper
	) {
		this.objectMapper = objectMapper;
		this.mc2Host = mc2Host;
		this.mc2Port = mc2Port;
	}


	public ListenableFuture<StompSession> connect() {
		val webSocketTransport = new WebSocketTransport(new StandardWebSocketClient());
		val sockJsClient = new SockJsClient(Collections.singletonList(webSocketTransport));
		sockJsClient.setMessageCodec(new Jackson2SockJsMessageCodec());
		val stompClient = new WebSocketStompClient(sockJsClient);
		val url = "ws://{host}:{port}/ws";
		return stompClient.connect(url, headers, new StompSessionHandlerAdapter() {
			@Override
			public void afterConnected(final StompSession stompSession, final StompHeaders stompHeaders) {
				// TODO: Log connect
			}
		}, mc2Host, mc2Port);
	}

	@SneakyThrows
	public void sendMessage(final StompSession stompSession, final MessageDto message) {
		stompSession.send("/app/message", objectMapper.writeValueAsString(message).getBytes());
	}
}
