package ru.prestu.microservices.mc1.controller;

import ru.prestu.microservices.mc1.service.MessageService;
import ru.prestu.microservices.model.MessageDto;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MessageController {

	MessageService messageService;

	@PostMapping(value = "/message", consumes = "application/json")
	public void saveMessage(@RequestBody final MessageDto message) {
		messageService.saveMessage(message);
	}
}
