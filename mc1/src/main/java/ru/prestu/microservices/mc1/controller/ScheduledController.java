package ru.prestu.microservices.mc1.controller;

import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStartedException;
import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStoppedException;
import ru.prestu.microservices.mc1.service.ScheduledService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@RestController
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ScheduledController {

	ScheduledService scheduledService;

	@GetMapping("/start")
	public ResponseEntity<String> start() {
		try {
			scheduledService.start();
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (SchedulerIsAlreadyStartedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
	}

	@GetMapping("/stop")
	public ResponseEntity<String> stop() {
		try {
			scheduledService.stop();
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (SchedulerIsAlreadyStoppedException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
		}
	}
}
