package ru.prestu.microservices.mc1.task;

import ru.prestu.microservices.mc1.service.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Mc1Task implements Task {

	@Autowired
	MessageService messageService;
	
	@Override
	public void run() {
		messageService.createMessage();
	}
}
