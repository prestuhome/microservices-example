package ru.prestu.microservices.mc1.service;

import ru.prestu.microservices.mc1.component.SchedulerStateChecker;
import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStartedException;
import ru.prestu.microservices.mc1.model.exception.SchedulerIsAlreadyStoppedException;
import ru.prestu.microservices.mc1.task.Task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import lombok.val;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ScheduledService {

	Integer delay;
	TaskScheduler taskScheduler;
	Task task;
	AtomicReference<ScheduledFuture<?>> futureReference = new AtomicReference<>();

	SchedulerStateChecker schedulerStateChecker;

	public ScheduledService(
			final @Value("${delay}") Integer delayInSeconds,
			final TaskScheduler taskScheduler,
			final Task task,
			final SchedulerStateChecker schedulerStateChecker
	) {
		this.delay = delayInSeconds * 1000;
		this.taskScheduler = taskScheduler;
		this.task = task;
		this.schedulerStateChecker = schedulerStateChecker;
	}

	public void start() throws SchedulerIsAlreadyStartedException {
		schedulerStateChecker.checkSchedulerStateOnStart(futureReference.get());
		futureReference.set(taskScheduler.scheduleAtFixedRate(task, delay));
	}

	public void stop() throws SchedulerIsAlreadyStoppedException {
		val scheduledFuture = futureReference.get();
		schedulerStateChecker.checkSchedulerStateOnStop(scheduledFuture);
		futureReference.get().cancel(false);
	}
}
