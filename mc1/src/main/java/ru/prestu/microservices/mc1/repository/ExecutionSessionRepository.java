package ru.prestu.microservices.mc1.repository;

import ru.prestu.microservices.mc1.model.domain.ExecutionSession;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ExecutionSessionRepository extends CrudRepository<ExecutionSession, Integer> {

	@Transactional
	@Modifying
	@Query("""
		update ExecutionSession session
		set session.messageAmount = session.messageAmount + 1
		where session.id = :sessionId
	""")
	void incrementMessageAmount(final Integer sessionId);
}
