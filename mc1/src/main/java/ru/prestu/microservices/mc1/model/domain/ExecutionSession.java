package ru.prestu.microservices.mc1.model.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = ExecutionSession.TABLE_NAME)
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ExecutionSession {

	public static final String TABLE_NAME = "execution_session";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	Integer id;
	@Column(name = "start_time")
	Long startTime;
	@Column(name = "end_time")
	Long endTime;
	@Column(name = "message_amount")
	Integer messageAmount;
}
