create table message (
    id int auto_increment primary key not null,
    session_id int not null,
    mc1_timestamp timestamp not null,
    mc2_timestamp timestamp not null,
    mc3_timestamp timestamp not null,
    end_timestamp timestamp not null
) engine=InnoDB