update execution_session
set message_amount = 0
where message_amount is null;

alter table execution_session modify message_amount int default 0;
