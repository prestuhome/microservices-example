create table execution_session (
    id int auto_increment primary key not null,
    start_time bigint,
    end_time bigint,
    message_amount int
) engine=InnoDB;

alter table message add constraint fk_message_session 
    foreign key (session_id) references execution_session (id) on delete cascade on update cascade;
