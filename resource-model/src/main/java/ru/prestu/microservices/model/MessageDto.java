package ru.prestu.microservices.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageDto {
	Integer id;
	Integer sessionId;
	Date mc1Timestamp;
	Date mc2Timestamp;
	Date mc3Timestamp;
	Date endTimestamp;
}
